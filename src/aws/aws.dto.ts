export interface Aws {
  accessKeyId: string;
  secretAccessKey: string;
  region: string;
}
export interface S3Dto {
  key: string;
  bucket: string;
}
