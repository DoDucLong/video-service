import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import AwsConfig from '../config/awsConfig';
import { AwsService } from './aws.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [AwsConfig],
    }),
  ],
  providers: [AwsService],
  exports: [AwsService],
})
export class AwsModule {}
