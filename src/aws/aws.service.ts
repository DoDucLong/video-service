import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as AWS from 'aws-sdk';
import { S3Dto } from './aws.dto';

@Injectable()
export class AwsService {
  private s3: any;
  private s3Dto: S3Dto;
  constructor() {
    this.s3 = new AWS.S3({
      accessKeyId: process.env.ACCESS_KEY_ID,
      secretAccessKey: process.env.SECRET_ACCESS_KEY,
      useAccelerateEndpoint: true,
    });
  }
  puObject(s3Dto: S3Dto): Promise<String> {
    const params = {
      Bucket: s3Dto.bucket,
      Key: s3Dto.key,
      Expires: 3 * 60, // 3 minutes
      ContentType: 'video/webm',
    };
    return this.s3.getSignedUrl('putObject', params);
  }
}
