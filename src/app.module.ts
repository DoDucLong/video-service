import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { VideosController } from './videos/videos.controller';
import { VideosService } from './videos/videos.service';
import { AwsService } from './aws/aws.service';
import { AwsModule } from './aws/aws.module';
import { VideosModule } from './videos/videos.module';
@Module({
  imports: [VideosModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
