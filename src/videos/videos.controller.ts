import { Controller, Get, Post, Body, Next } from '@nestjs/common';
import { S3Dto } from 'src/aws/aws.dto';
import { VideosService } from './videos.service';
@Controller('video')
export class VideosController {
  constructor(private readonly videoService: VideosService) {}

  @Get('')
  findAll(): string {
    return 'This action returns all cats';
  }

  @Get('gen-presigned-url')
  getPresignedUrl(): string {
    return 'This action returns all cats';
    // return await this.videoService.getPresignedUrl();
  }
}
