export interface Videos {
    uuid: string;
    videoFomat: string;
    caption: string;
    quality: string;
    size: string;
    videoPath: string;
    createAt: Date;
    createBy: string;
    updateAt: Date;
    updateBy: String;
    postId: string
}
