import { Module } from '@nestjs/common';
import { VideosService } from './videos.service';
import { VideosController } from './videos.controller';
import { AwsModule } from '../aws/aws.module';
@Module({
  imports: [AwsModule],
  providers: [VideosService],
  controllers: [VideosController],
})
export class VideosModule {}
