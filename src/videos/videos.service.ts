import { Injectable } from '@nestjs/common';
import { Aws } from 'src/aws/aws.dto';
import { v4 as uuidv4 } from 'uuid';
import { AwsService } from '../aws/aws.service';

@Injectable()
export class VideosService {
  constructor(private readonly awsServices: AwsService) {}

  async getPresignedUrl(){
    const uuid = uuidv4();
    return uuid;
    // try {
    //   const s3Dto = {
    //     key: uuid,
    //     bucket: process.env.S3_BUCKET,
    //   };
    //   const preSignedUrl = this.awsServices.puObject(s3Dto);
      
    //   preSignedUrl.then(function(url) {
    //     return url;
    //     },function(err) { 
    //         return err
    //     });
    // } catch (error) {
    //   return error;
    // }
  }
}
