require('dotenv-safe').config();
const { accessKeyId, secretAccessKey } = require('./vars');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
    accessKeyId,
    secretAccessKey,
    useAccelerateEndpoint: true 
});
AWS.config.update({
    
})