import { Injectable } from '@nestjs/common';
import s3 from '../config/s3.config';

exports.putObject = () => {
  return 'ok';
  const params = {
    Bucket: 'aibles-videos',
    Key: 'my-awesome-object.webm',
    Expires: 3 * 60, // 3 minutes
    ContentType: 'video/webm',
  };
  s3.getSignedUrl('putObject', params, (err, url) => {
    console.log(url);
    return url;
  });
};